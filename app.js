let bankBalance = 0;
let payBalance = 0;
let loanBalance = 0;
let price = document.getElementById("price");;
let myId;
let myValue;

function startCurrency() {
    changeCurrency("bankBalance", bankBalance);
    changeCurrency("loanBalance", loanBalance);
    changeCurrency("payBalance", payBalance);
}
// This function are useing number format to set kr after & set decimals to 0.
function changeCurrency(myId, myValue) {
    let currency = new Intl.NumberFormat('sv-SE', { minimumFractionDigits: 0, style: 'currency', currency: 'SEK' }).format(myValue);
    document.getElementById(`${myId}`).innerHTML = currency;
}
// This checks if the money in the bank. If not ypu can´t take a  loanBalance.
function getLoan() {
    if (loanBalance != 0) {
        alert("Can´t take a new loan util it´s payed back.");
    } else {
        if (bankBalance != 0) {
            let tempLoan = parseInt(prompt("Enter amount: "));            // Popup that you set your  loanBalance
            if (tempLoan <= bankBalance * 2 && loanBalance == 0 && tempLoan > 0) {
                loanBalance = tempLoan;
                bankBalance += loanBalance; 
                document.getElementById("loanBalance").innerText = loanBalance;
            } else if (isNaN(tempLoan)) {
                alert("It is not a number. Please enter a number next time");
            } else {
                alert("Not ok can´t loan more then double your balance or loan less than 0!");
            }
            checkLoan();
        } else {
            alert("Sorry no money, you need to work!");
        }
    }
}
// This function makes the repay button visible and invisible.
function checkLoan() {
    if (loanBalance > 0) {
        document.getElementById("btnRepay").style.display = "block";
    } else {
        document.getElementById("btnRepay").style.display = "none";
    }
}
// This add 100 kr to the work balance every time it get clicked.
function worked() {
    payBalance += 100;
    document.getElementById("payBalance").innerText = payBalance;
    checkLoan();
    changeCurrency("payBalance", payBalance);
}
// This checks if there is any money in pay balance so it can be set to 
// the bank balance.
function addBalance() {
    if (payBalance != 0) {
        // If there is money i pay balance to put in bank balance.
        // If there is a  loanBalance it will also give 10% of pay balance 
        // to  loanBalance balance.
        if (loanBalance != 0) {
            let fraction = payBalance * 0.1;
            if (loanBalance < fraction) {
                fraction = loanBalance;
                loanBalance = 0;
                payBalance - fraction;
            } else {
                loanBalance -= payBalance * 0.1;
                payBalance *= 0.9;
            }
            bankBalance += payBalance;
            payBalance = 0;
            document.getElementById("payBalance").innerText = payBalance;
            document.getElementById("loanBalance").innerText = loanBalance;
            document.getElementById("bankBalance").innerText = bankBalance;
        } else {
            bankBalance += payBalance;
            payBalance = 0;
            document.getElementById("payBalance").innerText = payBalance;
            document.getElementById("bankBalance").innerText = bankBalance;
        }
    } else {
        alert("Not enouh money 😥");
    }
    checkLoan();
}
// This pay the  loanBalance and if there is more money it will be
// added to bank balance.
function payLoan() {
    if (payBalance < loanBalance) {
        alert("You need to work more to pay all! Let´s go")
        loanBalance = loanBalance - payBalance;
        payBalance -= payBalance;
        document.getElementById("loanBalance").innerText = loanBalance;
        document.getElementById("payBalance").innerText = payBalance;
    } else {
        let toBank;
        toBank = loanBalance - payBalance;
        bankBalance -= toBank;
        loanBalance = 0;
        payBalance = 0;
        document.getElementById("loanBalance").innerText = loanBalance;
        document.getElementById("bankBalance").innerText = bankBalance;
        document.getElementById("payBalance").innerText = payBalance;
    }
    checkLoan();
}

function buyNow() {
    if (price <= bankBalance) {
        bankBalance -= price;
        document.getElementById("bankBalance").innerText = bankBalance;
        alert("You are now the owner of the new laptop. Nice choice thx u next!");
        changeCurrency("bankBalance", bankBalance);
    } else {
        alert("Not enough money!!!!!")
    }
}

// Variables that have the api.
const urlApi = "https://noroff-komputer-store-api.herokuapp.com/computers";
const imgUrlApi = "https://noroff-komputer-store-api.herokuapp.com/";

// Variables that´s connected to their ids. 
const computersElement = document.getElementById("computers");
const computerTitle = document.getElementById("computerTitle");
const computerDescription = document.getElementById("computerDescription");
const computerImg = document.getElementById("computerImg");
const computerFeature = document.getElementById("feature");

let featureObjects;
let computers = [];

// Fetch the api so I can use the data from it.
fetch(urlApi)
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToDropdown(computers))

// Collects all computers by an iteration. 
// Also adds a <br> to the features and adds price. 
const addComputersToDropdown = (computers) => {
    computers.forEach(x => addComputerToDropdown(x));
    featureObjects = computers[0].specs;
    document.getElementById("feature").innerHTML = featureObjects.join("<br>") + "<br>";
    price = computers[0].price;
    document.getElementById("price").innerHTML = price + " SEK";
    computerTitle.innerText = computers[0].title;
    computerDescription.innerText = computers[0].description;
    computerImg.src = imgUrlApi + computers[0].image;
    startCurrency();
}
// All computers will be created in a new element option so it will be added i the dropdown.
const addComputerToDropdown = (computer) => {
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computersElement.appendChild(computerElement);
}
// This handle when a change is made on the dropdown and updates the html.
const handleComputerDropdownChange = e => {
    const selectedComputer = computers[e.target.selectedIndex];
    computerTitle.innerText = selectedComputer.title;
    computerDescription.innerText = selectedComputer.description;
    computerImg.src = imgUrlApi + selectedComputer.image;
    computerFeature.innerHTML = selectedComputer.specs.join("<br>") + "<br>";
    price = selectedComputer.price;
    document.getElementById("price").innerHTML = selectedComputer.price + " SEK";
}

computersElement.addEventListener("change", handleComputerDropdownChange);


